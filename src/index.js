'use strict';

const app = require('express')();

const port = process.env.PORT || 8080;

app.get('/', (request, response) => {
  response.send('Inter-Planetary File System');
});

app.listen(port, () => {
  console.log(`App listening at Port ${port}.`);
});
